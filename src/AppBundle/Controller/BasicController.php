<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Reader;
use AppBundle\Form\RegistrationType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class BasicController extends Controller
{
    /**
     * @Route("/register")
     */
    public function registerAction(Request $request)
    {
        $reader = new Reader();

        $form = $this->createForm(RegistrationType::class, $reader);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $reader = $form->getData();
            $libraryCard = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
            $reader->setLibraryCard($libraryCard);
            $em = $this->getDoctrine()->getManager();
            $em->persist($reader);
            $em->flush();

            return $this->redirectToRoute("app_basic_getlibrarycard", [
                "id" => $reader->getId()
            ]);


        }

        return $this->render('@App/Basic/register.html.twig', array(
            'form' => $form->createView(),
        ));

    }

    /**
     * @Route("/{id}", requirements={"id": "\d+"})
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getLibraryCard(int $id)
    {
        $reader = $this->getDoctrine()
            ->getRepository('AppBundle:Reader')
            ->find($id);

        return $this->render('@App/Basic/getLibraryCard.html.twig', array(
            'reader' => $reader
        ));
    }


    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $books = $this->getDoctrine()
            ->getRepository('AppBundle:Book')
            ->findAll();

        return $this->render('@App/Basic/index.html.twig', array(
            'books' => $books
        ));

    }

//    public function getBook($book){
//
//        $readers = $this->getDoctrine()->getRepository('AppBundle:Reader')->findAll();
//
//        foreach ($readers as $reader){
//           if($reader->getLibraryCard() == $libCard){
//               $reader->addBook($book);
//           }
//
//
//        }



    }

}