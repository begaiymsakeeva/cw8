<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Reader;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadReaderData implements FixtureInterface, ORMFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $reader = new Reader();
        $libraryCard = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
        $reader
            ->setFullName('Иванов Иван')
            ->setAddress('Школьная, 2')
            ->setPassport('AN123456')
            ->setLibraryCard($libraryCard);

        $manager->persist($reader);

        $reader = new Reader();
        $libraryCard = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
        $reader
            ->setFullName('Петров Петр')
            ->setAddress('Школьная, 3')
            ->setPassport('AN123457')
            ->setLibraryCard($libraryCard);

        $manager->persist($reader);

        $reader = new Reader();
        $libraryCard = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
        $reader
            ->setFullName('Степанов Степан')
            ->setAddress('Школьная, 3')
            ->setPassport('AN123458')
            ->setLibraryCard($libraryCard);

        $manager->persist($reader);

        $reader = new Reader();
        $libraryCard = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
        $reader
            ->setFullName('Романов Роман')
            ->setAddress('Школьная, 4')
            ->setPassport('AN123459')
            ->setLibraryCard($libraryCard);

        $manager->persist($reader);

        $reader = new Reader();
        $libraryCard = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
        $reader
            ->setFullName('Романова Роза')
            ->setAddress('Школьная, 5')
            ->setPassport('AN1234510')
            ->setLibraryCard($libraryCard);

        $manager->persist($reader);

        $reader = new Reader();
        $libraryCard = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
        $reader
            ->setFullName('СТепанова Ольга')
            ->setAddress('Школьная, 6')
            ->setPassport('AN123411')
            ->setLibraryCard($libraryCard);

        $manager->persist($reader);
        $manager->flush();
    }
}
