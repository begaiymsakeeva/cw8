<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Book;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadBookData implements FixtureInterface, ORMFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $book = new Book();
        $book
            ->setName('Собачье середце.')
            ->setAuthor('Михаил Булгаков')
            ->setImage("book1.jpg");

        $manager->persist($book);

        $book = new Book();
        $book
            ->setName('Тонкое искусство пофигизма. Парадоксальный способ жить счастливо.')
            ->setAuthor('Марк Мэнсон')
            ->setImage("book2.jpg");

        $manager->persist($book);

        $book = new Book();
        $book
            ->setName('Гарри Поттер и Тайная комната.')
            ->setAuthor('Джоан К.Роулинг')
            ->setImage("book3.jpg");

        $manager->persist($book);

        $book = new Book();
        $book
            ->setName('Гарри Поттер и Философский камень.')
            ->setAuthor('Джоан К.Роулинг')
            ->setImage("book4.jpg");

        $manager->persist($book);

        $book = new Book();
        $book
            ->setName('Приключения Незнайки и его друзей.')
            ->setAuthor('Николай Носов')
            ->setImage("book5.jpg");

        $manager->persist($book);

        $book = new Book();
        $book
            ->setName('Сталин. Шаг Вправо.')
            ->setAuthor('Юрий Жуков')
            ->setImage("book6.jpg");

        $manager->persist($book);

        $book = new Book();
        $book
            ->setName('Хоббит.')
            ->setAuthor('Джон Р.Р. Толкин')
            ->setImage("book7.jpg");

        $manager->persist($book);

        $book = new Book();
        $book
            ->setName('Никита ищет море.')
            ->setAuthor('Дарья Варденбург')
            ->setImage("book8.jpg");

        $manager->persist($book);

        $book = new Book();
        $book
            ->setName('Сказки. Снежная Королева. Русалочка. Дюймовочка.')
            ->setAuthor('Ханс Кристиан Андерсен')
            ->setImage("book9.jpg");

        $manager->persist($book);

        $book = new Book();
        $book
            ->setName('Краткая история времени от Большого Взрыва до Черных Дыр.')
            ->setAuthor('Стивен Хокинг')
            ->setImage("book10.jpg");

        $manager->persist($book);

        $manager->flush();
    }
}
